/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.ui;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 *
 * @author capri
 */
public class StartPageController implements Initializable {

    @FXML
    private Label label;
    @FXML
    ImageView logo = new ImageView();

    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }

    @FXML
    private void crearCuenta(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/medallistaolimpico/ui/RegistryUI.fxml"));
        Parent parentScene = loader.load();
        RegistryController controller = loader.getController();

        Scene newScene = new Scene(parentScene);
        Stage window = (Stage) ((javafx.scene.Node) event.getSource()).getScene().getWindow();
        window.close();

        window.setScene(newScene);
        window.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Image image = new Image(this.getClass().getResourceAsStream("/medallistaolimpico/ui/media/logo.jpg"), 252, 169, true, true);
        logo.setImage(image);

    }

}
