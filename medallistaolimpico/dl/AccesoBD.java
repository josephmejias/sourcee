/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package medallistaolimpico.dl;

import java.sql.*;

public class AccesoBD {
    
    private Connection conn;
    private Statement stmt;
    
    public AccesoBD(String url, String user, String password)throws SQLException {
       conn = DriverManager.getConnection(url,user,password);
    }

    public ResultSet ejecutarSQL(String sqlQuery) throws SQLException{
        ResultSet rs;
        stmt = conn.createStatement();
        rs = stmt.executeQuery(sqlQuery);
        return rs;
    }

    public void ejecutarQuery(String sqlQuery) throws SQLException{
        stmt = conn.createStatement();
        stmt.executeUpdate(sqlQuery);
    }

}
