/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.dl;

import medallistaolimpico.util.Utility;

public class Conector {

    private static AccesoBD connectorBD;

    public static AccesoBD getConnector() throws Exception {
        String url = Utility.getDriver() + "//" + Utility.getServer() + "/" + Utility.getDataBase() + "?useSSL=false&serverTimezone=UTC";
        String user = Utility.getUser();
        String password = Utility.getPassword();

        if (connectorBD == null) {
            connectorBD = new AccesoBD(url, user, password);
        }

        return connectorBD;
    }

}
