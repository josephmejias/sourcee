/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Utility {

    private static final Properties p = new Properties();
    
    private static String getProperty(String property){
        try {
            p.load(new FileInputStream("src\\medallistaolimpico\\app.properties"));
            return p.getProperty(property);
        } catch (IOException ex) {
            return "error";
        }
    }

    /**
     * @return the driver
     */
    public static String getDriver() {
        return getProperty("driver");
    }

    /**
     * @return the server
     */
    public static String getServer() {
        return getProperty("server");
    }

    /**
     * @return the dataBase
     */
    public static String getDataBase() {
        return getProperty("dataBase");
    }

    /**
     * @return the user
     */
    public static String getUser() {
        return getProperty("user");
    }

    /**
     * @return the password
     */
    public static String getPassword() {
        return getProperty("password");
    }

    /**
     * @return the repository
     */
    public static String getRepository() {
        return getProperty("repository");
    }

}
