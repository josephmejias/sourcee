/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.tl;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author capri
 */
public class Controller extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        
        Parent root = FXMLLoader.load(getClass().getResource("/medallistaolimpico/ui/StartPage.fxml"));
        stage.setTitle("");
        stage.setScene(new Scene(root, 320, 654));
        
        stage.initStyle(StageStyle.UTILITY);
        stage.show();
        
    }

    public void runApp() {
        launch(Controller.class);
    }
    
}
