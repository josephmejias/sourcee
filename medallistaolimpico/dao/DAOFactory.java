/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package medallistaolimpico.dao;

import medallistaolimpico.bl.entidades.actividad.*;
import medallistaolimpico.bl.entidades.administrador.IAdministradorDAO;
import medallistaolimpico.bl.entidades.atleta.IAtletaDAO;
import medallistaolimpico.bl.entidades.canton.ICantonDAO;
import medallistaolimpico.bl.entidades.direccion.IDireccionDAO;
import medallistaolimpico.bl.entidades.distrito.IDistritoDAO;
import medallistaolimpico.bl.entidades.grupo.IGrupoDAO;
import medallistaolimpico.bl.entidades.hito.IHitoDAO;
import medallistaolimpico.bl.entidades.locacion.ILocacionDAO;
import medallistaolimpico.bl.entidades.medalla.IMedallaDAO;
import medallistaolimpico.bl.entidades.metodopago.IMetodoPagoDAO;
import medallistaolimpico.bl.entidades.pais.IPaisDAO;
import medallistaolimpico.bl.entidades.persona.IPersonaDAO;
import medallistaolimpico.bl.entidades.provincia.IProvinciaDAO;
import medallistaolimpico.bl.entidades.reto.IRetoDAO;
import medallistaolimpico.bl.entidades.transaccion.ITransaccionDAO;
import medallistaolimpico.util.Utility;


public abstract class DAOFactory {

    public static DAOFactory getDAOFactory(){
        try {


            switch (Utility.getRepository()){
                case "mysql":
                     return new MySQLDAOFactory();
                default: return null;
            }
        }catch(Exception e){
            return null;
        }
    }
    
    public abstract IActividadDAO getActividadDAO();
    public abstract IAdministradorDAO getAdministradorDAO();
    public abstract IAtletaDAO getAtletaDAO();
    public abstract ICantonDAO getCantonDAO();
    public abstract IDireccionDAO getDireccionDAO();
    public abstract IDistritoDAO getDistritoDAO();
    public abstract IGrupoDAO getGrupoDAO();
    public abstract IHitoDAO getHitoDAO();
    public abstract ILocacionDAO getLocacionDAO();
    public abstract IMedallaDAO getMedallaDAO();
    public abstract IMetodoPagoDAO getMetodoPagoDAO();
    public abstract IPaisDAO getPaisDAO();
    public abstract IPersonaDAO getPersonaDAO();
    public abstract IProvinciaDAO getProvinciaDAO();
    public abstract IRetoDAO getRetoDAO();
    public abstract ITransaccionDAO getTransaccionDAO();


}
