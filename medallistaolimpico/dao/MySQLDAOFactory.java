/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.dao;

import medallistaolimpico.bl.entidades.actividad.*;
import medallistaolimpico.bl.entidades.administrador.*;
import medallistaolimpico.bl.entidades.atleta.*;
import medallistaolimpico.bl.entidades.canton.*;
import medallistaolimpico.bl.entidades.direccion.*;
import medallistaolimpico.bl.entidades.distrito.*;
import medallistaolimpico.bl.entidades.grupo.*;
import medallistaolimpico.bl.entidades.hito.*;
import medallistaolimpico.bl.entidades.locacion.*;
import medallistaolimpico.bl.entidades.medalla.*;
import medallistaolimpico.bl.entidades.metodopago.*;
import medallistaolimpico.bl.entidades.pais.*;
import medallistaolimpico.bl.entidades.persona.*;
import medallistaolimpico.bl.entidades.provincia.*;
import medallistaolimpico.bl.entidades.reto.*;
import medallistaolimpico.bl.entidades.transaccion.*;

public class MySQLDAOFactory extends DAOFactory {

    @Override
    public IActividadDAO getActividadDAO() {
        return new MySQLActividadImpl();
    }

    @Override
    public IAdministradorDAO getAdministradorDAO() {
        return new MySQLAdministradorImpl();
    }

    @Override
    public IAtletaDAO getAtletaDAO() {
        return new MySQLAtletaImpl();
    }

    @Override
    public ICantonDAO getCantonDAO() {
        return new MySQLCantonImpl();
    }

    @Override
    public IDireccionDAO getDireccionDAO() {
        return new MySQLDireccionImpl();
    }

    @Override
    public IDistritoDAO getDistritoDAO() {
        return new MySQLDistritoImpl();
    }

    @Override
    public IGrupoDAO getGrupoDAO() {
        return new MySQLGrupoImpl();
    }

    @Override
    public IHitoDAO getHitoDAO() {
        return new MySQLHitoImpl();
    }

    @Override
    public ILocacionDAO getLocacionDAO() {
        return new MySQLLocacionImpl();
    }

    @Override
    public IMedallaDAO getMedallaDAO() {
        return new MySQLMedallaImpl();
    }

    @Override
    public IMetodoPagoDAO getMetodoPagoDAO() {
        return new MySQLMetodoPagoImpl();
    }

    @Override
    public IPaisDAO getPaisDAO() {
        return new MySQLPaisImpl();
    }

    @Override
    public IPersonaDAO getPersonaDAO() {
        return new MySQLPersonaImpl();
    }

    @Override
    public IProvinciaDAO getProvinciaDAO() {
        return new MySQLProvinciaImpl();
    }

    @Override
    public IRetoDAO getRetoDAO() {
        return new MySQLRetoImpl();
    }

    @Override
    public ITransaccionDAO getTransaccionDAO() {
        return new MySQLTransaccionImpl();
    }

}
