/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.bl.entidades.hito;

import java.util.ArrayList;

/**
 *
 * @author capri
 */
public interface IHitoDAO {
    
    public void registarHito(Hito hito) throws Exception;

    public ArrayList<Hito> listarHitos() throws Exception;

    public void modificarHito(Hito hito) throws Exception;

    public void eliminarHito(String codigo) throws Exception;
    
}
