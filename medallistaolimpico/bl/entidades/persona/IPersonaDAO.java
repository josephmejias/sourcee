/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.bl.entidades.persona;

import java.util.ArrayList;

/**
 *
 * @author capri
 */
public interface IPersonaDAO {
    
    public void registarPersona(Persona persona) throws Exception;

    public ArrayList<Persona> listarPersonas() throws Exception;

    public void modificarPersona(Persona persona) throws Exception;

    public void eliminarPersona(String codigo) throws Exception;
    
}
