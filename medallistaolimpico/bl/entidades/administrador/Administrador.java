/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package medallistaolimpico.bl.entidades.administrador;

import medallistaolimpico.bl.entidades.pais.Pais;
import medallistaolimpico.bl.entidades.persona.Persona;


public class Administrador extends Persona {

    public Administrador() {
    }

    public Administrador(int idPersona, String nombre, String segundoNombre, String apellidos, String cedula, Pais pais, String correo, String clave) {
        super(idPersona, nombre, segundoNombre, apellidos, cedula, pais, correo, clave);
    }

    @Override
    public String toString() {
        return "Administrador{" + super.toString() + '}';
    }
    
    

}
