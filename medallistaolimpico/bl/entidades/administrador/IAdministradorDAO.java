/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.bl.entidades.administrador;

import java.util.ArrayList;

/**
 *
 * @author capri
 */
public interface IAdministradorDAO {

    public void registarAdministrador(Administrador administrador) throws Exception;

    public ArrayList<Administrador> listarAdministradores() throws Exception;

    public void modificarAdministrador(Administrador administrador) throws Exception;

    public void eliminarAdministrador(String codigo) throws Exception;

}
