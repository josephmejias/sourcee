/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.bl.entidades.provincia;

import java.util.ArrayList;

/**
 *
 * @author capri
 */
public interface IProvinciaDAO {
    
    public void registarProvincia(Provincia provincia) throws Exception;

    public ArrayList<Provincia> listarProvincias() throws Exception;

    public void modificarProvincia(Provincia provincia) throws Exception;

    public void eliminarProvincia(String codigo) throws Exception;
    
}
