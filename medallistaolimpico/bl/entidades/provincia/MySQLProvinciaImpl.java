/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.bl.entidades.provincia;

import java.sql.ResultSet;
import java.util.ArrayList;
import medallistaolimpico.dl.Conector;

public class MySQLProvinciaImpl implements IProvinciaDAO {

    private String sqlQuery;

    @Override
    public void registarProvincia(Provincia provincia) throws Exception {
        sqlQuery = "";
        Conector.getConnector().ejecutarQuery(sqlQuery);
    }

    @Override
    public ArrayList<Provincia> listarProvincias() throws Exception {
        ResultSet rs = Conector.getConnector().ejecutarSQL(sqlQuery);
        return null;
    }

    @Override
    public void modificarProvincia(Provincia provincia) throws Exception {
        sqlQuery = "";
        Conector.getConnector().ejecutarQuery(sqlQuery);
    }

    @Override
    public void eliminarProvincia(String codigo) throws Exception {
        sqlQuery = "";
        Conector.getConnector().ejecutarQuery(sqlQuery);
    }

}
