/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.bl.entidades.locacion;

import java.util.ArrayList;

/**
 *
 * @author capri
 */
public interface ILocacionDAO {
    
    public void registarLocacion(Locacion locacion) throws Exception;

    public ArrayList<Locacion> listarLocaciones() throws Exception;

    public void modificarLocacion(Locacion locacion) throws Exception;

    public void eliminarLocacion(String codigo) throws Exception;
    
}
