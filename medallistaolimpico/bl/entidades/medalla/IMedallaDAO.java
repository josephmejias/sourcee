/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.bl.entidades.medalla;

import java.util.ArrayList;

/**
 *
 * @author capri
 */
public interface IMedallaDAO {
    
    public void registarMedalla(Medalla medalla) throws Exception;

    public ArrayList<Medalla> listarMedallas() throws Exception;

    public void modificarMedalla(Medalla medalla) throws Exception;

    public void eliminarMedalla(String codigo) throws Exception;
    
}
