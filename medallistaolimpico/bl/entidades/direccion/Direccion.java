/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package medallistaolimpico.bl.entidades.direccion;

import medallistaolimpico.bl.entidades.distrito.Distrito;
import medallistaolimpico.bl.entidades.pais.Pais;
import medallistaolimpico.bl.entidades.provincia.Provincia;
import medallistaolimpico.bl.entidades.canton.Canton;


public class Direccion {
    
    private Pais pais;
    private Provincia provincia;
    private Canton canton;
    private Distrito distrito;
    private String nombreDireccion;
    private String linea1;
    private String linea2;

    @Override
    public String toString() {
        return "Direccion{" + "pais=" + pais + ", provincia=" + provincia + ", canton=" + canton + ", distrito=" + distrito + ", nombreDireccion=" + nombreDireccion + ", linea1=" + linea1 + ", linea2=" + linea2 + '}';
    }
    
    

    public Direccion() {
    }

    public Direccion(Pais pais, Provincia provincia, Canton canton, Distrito distrito, String nombreDireccion, String linea1, String linea2) {
        this.pais = pais;
        this.provincia = provincia;
        this.canton = canton;
        this.distrito = distrito;
        this.nombreDireccion = nombreDireccion;
        this.linea1 = linea1;
        this.linea2 = linea2;
    }
    

    /**
     * @return the pais
     */
    public Pais getPais() {
        return pais;
    }

    /**
     * @param pais the pais to set
     */
    public void setPais(Pais pais) {
        this.pais = pais;
    }

    /**
     * @return the provincia
     */
    public Provincia getProvincia() {
        return provincia;
    }

    /**
     * @param provincia the provincia to set
     */
    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }

    /**
     * @return the canton
     */
    public Canton getCanton() {
        return canton;
    }

    /**
     * @param canton the canton to set
     */
    public void setCanton(Canton canton) {
        this.canton = canton;
    }

    /**
     * @return the distrito
     */
    public Distrito getDistrito() {
        return distrito;
    }

    /**
     * @param distrito the distrito to set
     */
    public void setDistrito(Distrito distrito) {
        this.distrito = distrito;
    }

    /**
     * @return the nombreDireccion
     */
    public String getNombreDireccion() {
        return nombreDireccion;
    }

    /**
     * @param nombreDireccion the nombreDireccion to set
     */
    public void setNombreDireccion(String nombreDireccion) {
        this.nombreDireccion = nombreDireccion;
    }

    /**
     * @return the linea1
     */
    public String getLinea1() {
        return linea1;
    }

    /**
     * @param linea1 the linea1 to set
     */
    public void setLinea1(String linea1) {
        this.linea1 = linea1;
    }

    /**
     * @return the linea2
     */
    public String getLinea2() {
        return linea2;
    }

    /**
     * @param linea2 the linea2 to set
     */
    public void setLinea2(String linea2) {
        this.linea2 = linea2;
    }

}
