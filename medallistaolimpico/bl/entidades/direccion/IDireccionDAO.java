/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.bl.entidades.direccion;

import java.util.ArrayList;

/**
 *
 * @author capri
 */
public interface IDireccionDAO {
    
    public void registarDireccion(Direccion direccion) throws Exception;

    public ArrayList<Direccion> listarAtletas() throws Exception;

    public void modificarDireccion(Direccion direccion) throws Exception;

    public void eliminarDireccion(String codigo) throws Exception;
    
}
