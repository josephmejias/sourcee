/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.bl.entidades.distrito;

import java.util.ArrayList;

/**
 *
 * @author capri
 */
public interface IDistritoDAO {
    
    public void registarDistrito(Distrito distrito) throws Exception;

    public ArrayList<Distrito> listarDistritos() throws Exception;

    public void modificarDistrito(Distrito distrito) throws Exception;

    public void eliminarDistrito(String codigo) throws Exception;
    
}
