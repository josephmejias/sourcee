/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.bl.entidades.pais;

import java.util.ArrayList;

/**
 *
 * @author capri
 */
public interface IPaisDAO {
    
    public void registarMetodoPago(Pais pais) throws Exception;

    public ArrayList<Pais> listarPaises() throws Exception;

    public void modificarPais(Pais pais) throws Exception;

    public void eliminarPais(String codigo) throws Exception;
    
}
