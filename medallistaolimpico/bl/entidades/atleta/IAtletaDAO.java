/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.bl.entidades.atleta;

import java.util.ArrayList;

/**
 *
 * @author capri
 */
public interface IAtletaDAO {
    
    public void registarAtleta(Atleta atleta) throws Exception;

    public ArrayList<Atleta> listarAtletas() throws Exception;

    public void modificarAtleta(Atleta atleta) throws Exception;

    public void eliminarAtleta(String codigo) throws Exception;
    
}
