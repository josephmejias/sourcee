/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.bl.entidades.metodopago;

import java.sql.ResultSet;
import java.util.ArrayList;
import medallistaolimpico.dl.Conector;

public class MySQLMetodoPagoImpl implements IMetodoPagoDAO {

    private String sqlQuery;

    @Override
    public void registarMetodoPago(MetodoPago metodoPago) throws Exception {
        sqlQuery = "";
        Conector.getConnector().ejecutarQuery(sqlQuery);
    }

    @Override
    public ArrayList<MetodoPago> listarMetodosPago() throws Exception {
        ResultSet rs = Conector.getConnector().ejecutarSQL(sqlQuery);
        return null;
    }

    @Override
    public void modificarMetodoPago(MetodoPago metodoPago) throws Exception {
        sqlQuery = "";
        Conector.getConnector().ejecutarQuery(sqlQuery);
    }

    @Override
    public void eliminarMetodoPago(String codigo) throws Exception {
        sqlQuery = "";
        Conector.getConnector().ejecutarQuery(sqlQuery);
    }

}
