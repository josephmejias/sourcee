/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.bl.entidades.metodopago;

import java.util.ArrayList;

/**
 *
 * @author capri
 */
public interface IMetodoPagoDAO {
    
    public void registarMetodoPago(MetodoPago metodoPago) throws Exception;

    public ArrayList<MetodoPago> listarMetodosPago() throws Exception;

    public void modificarMetodoPago(MetodoPago metodoPago) throws Exception;

    public void eliminarMetodoPago(String codigo) throws Exception;
    
}
