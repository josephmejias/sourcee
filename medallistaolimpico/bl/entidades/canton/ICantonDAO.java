/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.bl.entidades.canton;

import java.util.ArrayList;

/**
 *
 * @author capri
 */
public interface ICantonDAO {
    
    public void registarCanton(Canton canton) throws Exception;

    public ArrayList<Canton> listarCantones() throws Exception;

    public void modificarCanton(Canton canton) throws Exception;

    public void eliminarCanton(String codigo) throws Exception;
    
}
