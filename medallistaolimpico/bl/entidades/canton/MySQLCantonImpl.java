/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.bl.entidades.canton;

import java.sql.ResultSet;
import java.util.ArrayList;
import medallistaolimpico.dl.Conector;

public class MySQLCantonImpl implements ICantonDAO {

    private String sqlQuery;

    @Override
    public void registarCanton(Canton canton) throws Exception {
        sqlQuery = "";
        Conector.getConnector().ejecutarQuery(sqlQuery);
    }

    @Override
    public ArrayList<Canton> listarCantones() throws Exception {
        ResultSet rs = Conector.getConnector().ejecutarSQL(sqlQuery);
        return null;
    }

    @Override
    public void modificarCanton(Canton canton) throws Exception {
        sqlQuery = "";
        Conector.getConnector().ejecutarQuery(sqlQuery);
    }

    @Override
    public void eliminarCanton(String codigo) throws Exception {
        sqlQuery = "";
        Conector.getConnector().ejecutarQuery(sqlQuery);
    }

}
