/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.bl.entidades.actividad;

import java.util.ArrayList;

/**
 *
 * @author capri
 */
public interface IActividadDAO {

    public void registarActividad(Actividad actividad) throws Exception;

    public ArrayList<Actividad> listarActividades() throws Exception;

    public void modificarActividad(Actividad actividad) throws Exception;

    public void eliminarActividad(String codigo) throws Exception;

}
