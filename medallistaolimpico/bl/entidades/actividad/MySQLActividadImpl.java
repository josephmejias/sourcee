/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.bl.entidades.actividad;

import java.sql.ResultSet;
import java.util.ArrayList;
import medallistaolimpico.dl.Conector;

public class MySQLActividadImpl implements IActividadDAO {

    private String sqlQuery;

    @Override
    public void registarActividad(Actividad actividad) throws Exception {
        sqlQuery = "insert into actividad(codigo, nombre, rutaicono)"
                + "values (" + actividad.getCodigo() + "," + actividad.getNombre() + "," + actividad.getRutaIcono() + ")";
        Conector.getConnector().ejecutarQuery(sqlQuery);
    }

    @Override
    public ArrayList<Actividad> listarActividades() throws Exception {
        sqlQuery = "select * from actividad";
        ArrayList<Actividad> lista = new ArrayList();
        ResultSet rs = Conector.getConnector().ejecutarSQL(sqlQuery);
        while(rs.next()){
            Actividad actividad = new Actividad();
            actividad.setIdActividad(rs.getInt("idactividad"));
            actividad.setCodigo(rs.getString("codigo"));
            actividad.setNombre(rs.getString("nombre"));
            actividad.setRutaIcono(rs.getString("rutaicono"));
            lista.add(actividad);
        }
        return lista;
    }

    @Override
    public void modificarActividad(Actividad actividad) throws Exception {
        sqlQuery = "update actividad set codigo =" + actividad.getCodigo() + ", nombre=" + actividad.getNombre() 
                + ", rutaicono=" + actividad.getRutaIcono() + " where idactividad like " + actividad.getIdActividad();
        Conector.getConnector().ejecutarQuery(sqlQuery);
    }

    @Override
    public void eliminarActividad(String codigo) throws Exception {
        sqlQuery = "delete from actividad where codigo like" + codigo;
        Conector.getConnector().ejecutarQuery(sqlQuery);
    }

}
