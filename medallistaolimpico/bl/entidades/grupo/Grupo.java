/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package medallistaolimpico.bl.entidades.grupo;

import medallistaolimpico.bl.entidades.atleta.Atleta;
import java.time.LocalDate;
import java.util.ArrayList;
import medallistaolimpico.bl.entidades.reto.Reto;


public class Grupo {
    
    private int idGrupo;
    private String nombreGrupo;
    private ArrayList<Atleta> miembros;
    private Reto reto;
    private LocalDate fechaRegistro;

    public Grupo() {
    }

    public Grupo(int idGrupo, String nombreGrupo, ArrayList<Atleta> miembros, Reto reto, LocalDate fechaRegistro) {
        this.idGrupo = idGrupo;
        this.nombreGrupo = nombreGrupo;
        this.miembros = miembros;
        this.reto = reto;
        this.fechaRegistro = fechaRegistro;
    }

    /**
     * @return the idGrupo
     */
    public int getIdGrupo() {
        return idGrupo;
    }

    /**
     * @param idGrupo the idGrupo to set
     */
    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }

    /**
     * @return the nombreGrupo
     */
    public String getNombreGrupo() {
        return nombreGrupo;
    }

    /**
     * @param nombreGrupo the nombreGrupo to set
     */
    public void setNombreGrupo(String nombreGrupo) {
        this.nombreGrupo = nombreGrupo;
    }

    /**
     * @return the miembros
     */
    public ArrayList<Atleta> getMiembros() {
        return miembros;
    }

    /**
     * @param miembros the miembros to set
     */
    public void setMiembros(ArrayList<Atleta> miembros) {
        this.miembros = miembros;
    }

    /**
     * @return the reto
     */
    public Reto getReto() {
        return reto;
    }

    /**
     * @param reto the reto to set
     */
    public void setReto(Reto reto) {
        this.reto = reto;
    }

    /**
     * @return the fechaRegistro
     */
    public LocalDate getFechaRegistro() {
        return fechaRegistro;
    }

    /**
     * @param fechaRegistro the fechaRegistro to set
     */
    public void setFechaRegistro(LocalDate fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
    

}
