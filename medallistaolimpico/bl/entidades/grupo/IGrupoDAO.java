/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.bl.entidades.grupo;

import java.util.ArrayList;

/**
 *
 * @author capri
 */
public interface IGrupoDAO {
    
    
    public void registarGrupo(Grupo grupo) throws Exception;

    public ArrayList<Grupo> listarDistritos() throws Exception;

    public void modificarGrupo(Grupo grupo) throws Exception;

    public void eliminarGrupo(String codigo) throws Exception;
    
}
