/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package medallistaolimpico.bl.entidades.transaccion;

import medallistaolimpico.bl.entidades.metodopago.MetodoPago;
import medallistaolimpico.bl.entidades.atleta.Atleta;
import java.time.LocalDate;


public class Transaccion {
    
    private int idTransaccion;
    private MetodoPago pago;
    private Atleta atleta;
    private LocalDate fecha;
    private String estado;
    private String tipoCambio;
    private double monto;
    private String tipoTransaccion;

    @Override
    public String toString() {
        return "Transaccion{" + "idTransaccion=" + idTransaccion + ", pago=" + pago + ", atleta=" + atleta + ", fecha=" + fecha + ", estado=" + estado + ", tipoCambio=" + tipoCambio + ", monto=" + monto + ", tipoTransaccion=" + tipoTransaccion + '}';
    }

    public Transaccion(int idTransaccion, MetodoPago pago, Atleta atleta, LocalDate fecha, String estado, String tipoCambio, double monto, String tipoTransaccion) {
        this.idTransaccion = idTransaccion;
        this.pago = pago;
        this.atleta = atleta;
        this.fecha = fecha;
        this.estado = estado;
        this.tipoCambio = tipoCambio;
        this.monto = monto;
        this.tipoTransaccion = tipoTransaccion;
    }

    public Transaccion() {
    }

    /**
     * @return the idTransaccion
     */
    public int getIdTransaccion() {
        return idTransaccion;
    }

    /**
     * @param idTransaccion the idTransaccion to set
     */
    public void setIdTransaccion(int idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    /**
     * @return the pago
     */
    public MetodoPago getPago() {
        return pago;
    }

    /**
     * @param pago the pago to set
     */
    public void setPago(MetodoPago pago) {
        this.pago = pago;
    }

    /**
     * @return the atleta
     */
    public Atleta getAtleta() {
        return atleta;
    }

    /**
     * @param atleta the atleta to set
     */
    public void setAtleta(Atleta atleta) {
        this.atleta = atleta;
    }

    /**
     * @return the fecha
     */
    public LocalDate getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the tipoCambio
     */
    public String getTipoCambio() {
        return tipoCambio;
    }

    /**
     * @param tipoCambio the tipoCambio to set
     */
    public void setTipoCambio(String tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    /**
     * @return the monto
     */
    public double getMonto() {
        return monto;
    }

    /**
     * @param monto the monto to set
     */
    public void setMonto(double monto) {
        this.monto = monto;
    }

    /**
     * @return the tipoTransaccion
     */
    public String getTipoTransaccion() {
        return tipoTransaccion;
    }

    /**
     * @param tipoTransaccion the tipoTransaccion to set
     */
    public void setTipoTransaccion(String tipoTransaccion) {
        this.tipoTransaccion = tipoTransaccion;
    }

}
