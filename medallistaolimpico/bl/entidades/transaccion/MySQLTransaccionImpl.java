/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.bl.entidades.transaccion;

import java.sql.ResultSet;
import java.util.ArrayList;
import medallistaolimpico.dl.Conector;

public class MySQLTransaccionImpl implements ITransaccionDAO {

    private String sqlQuery;

    @Override
    public void registarTransaccion(Transaccion reto) throws Exception {
        sqlQuery = "";
        Conector.getConnector().ejecutarQuery(sqlQuery);
    }

    @Override
    public ArrayList<Transaccion> listarTransacciones() throws Exception {
        ResultSet rs = Conector.getConnector().ejecutarSQL(sqlQuery);
        return null;
    }

    @Override
    public void modificarTransaccion(Transaccion reto) throws Exception {
        sqlQuery = "";
        Conector.getConnector().ejecutarQuery(sqlQuery);
    }

    @Override
    public void eliminarTransaccion(String codigo) throws Exception {
        sqlQuery = "";
        Conector.getConnector().ejecutarQuery(sqlQuery);
    }

}
