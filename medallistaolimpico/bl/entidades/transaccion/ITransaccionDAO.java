/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.bl.entidades.transaccion;

import java.util.ArrayList;

/**
 *
 * @author capri
 */
public interface ITransaccionDAO {
    
    public void registarTransaccion(Transaccion reto) throws Exception;

    public ArrayList<Transaccion> listarTransacciones() throws Exception;

    public void modificarTransaccion(Transaccion reto) throws Exception;

    public void eliminarTransaccion(String codigo) throws Exception;
    
}
