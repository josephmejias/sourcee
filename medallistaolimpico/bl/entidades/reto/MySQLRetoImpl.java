/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.bl.entidades.reto;

import java.sql.ResultSet;
import java.util.ArrayList;
import medallistaolimpico.dl.Conector;

public class MySQLRetoImpl implements IRetoDAO {

    private String sqlQuery;

    @Override
    public void registarReto(Reto reto) throws Exception {
        sqlQuery = "";
        Conector.getConnector().ejecutarQuery(sqlQuery);
    }

    @Override
    public ArrayList<Reto> listarRetos() throws Exception {
        ResultSet rs = Conector.getConnector().ejecutarSQL(sqlQuery);
        return null;
    }

    @Override
    public void modificarReto(Reto reto) throws Exception {
        sqlQuery = "";
        Conector.getConnector().ejecutarQuery(sqlQuery);
    }

    @Override
    public void eliminarReto(String codigo) throws Exception {
        sqlQuery = "";
        Conector.getConnector().ejecutarQuery(sqlQuery);
    }

}
