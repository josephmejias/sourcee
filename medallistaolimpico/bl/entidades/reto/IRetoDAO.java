/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.bl.entidades.reto;

import java.util.ArrayList;

/**
 *
 * @author capri
 */
public interface IRetoDAO {
    
    public void registarReto(Reto reto) throws Exception;

    public ArrayList<Reto> listarRetos() throws Exception;

    public void modificarReto(Reto reto) throws Exception;

    public void eliminarReto(String codigo) throws Exception;
    
}
