/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medallistaolimpico.bl.logic;

import java.util.ArrayList;
import medallistaolimpico.bl.entidades.actividad.Actividad;
import medallistaolimpico.bl.entidades.actividad.IActividadDAO;
import medallistaolimpico.dao.DAOFactory;

public class ActividadGestor {

    private final DAOFactory factory;
    private final IActividadDAO dao;

    public ActividadGestor() {
        factory = DAOFactory.getDAOFactory();
        dao = factory.getActividadDAO();
    }

    public void registrarActividad(String codigo, String nombre, String rutaIcono) throws Exception {
        Actividad actividad = new Actividad(codigo, nombre, rutaIcono);
        dao.registarActividad(actividad);
    }

    public void modificarActividad(String codigo, String nombre, String rutaIcono) throws Exception {
        Actividad actividad = new Actividad(codigo, nombre, rutaIcono);
        dao.modificarActividad(actividad);
    }

    public void eliminarActividad(String codigo) throws Exception {
        dao.eliminarActividad(codigo);
    }

    public ArrayList<Actividad> listarActividades() throws Exception {
        return dao.listarActividades();
    }

}
